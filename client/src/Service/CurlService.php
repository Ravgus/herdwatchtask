<?php

namespace App\Service;

class CurlService
{
    private $url;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * Prepare request data
     * @param string $method
     * @param array $data
     * @return bool|false|resource
     */
    private function prepare(string $method, array $data = [])
    {
        if (!in_array($method, ['GET', 'POST', 'PUT', 'DELETE'])) {
            return false;
        }

        $ch = curl_init();

        if ($ch === false)
            return false;

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

        if ($method === 'POST' || $method === 'PUT') {
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        return $ch;
    }

    /**
     * Transform response data
     * @param string $data
     * @param $header
     * @return false|string
     */
    private function transformData(string $data, $header)
    {
        $data = json_decode($data, true);

        if (is_null($data)) {
            return $this->getError();
        } elseif (array_key_exists('error', $data)) {
            $result['error']['message'] = $data['error']['message'] ?? null;
            $result['error']['code'] = $header;
        } else {
            $result = $data;
        }

        return json_encode($result);
    }

    /**
     * Return error message
     * @return false|string
     */
    private function getError()
    {
        $result['error']['message'] = 'Something went wrong, please try again';
        $result['error']['code'] = 400;

        return json_encode($result);
    }

    /**
     * Makes request to point of destination
     * @param string $method
     * @param array $data
     * @return bool|string
     */
    public function getResponse(string $method, array $data = [])
    {
        $ch = $this->prepare($method, $data);

        if ($ch === false)
            return $this->getError();

        curl_setopt($ch, CURLOPT_HEADER, false);

        $result = curl_exec($ch);

        if (is_string($result)) {
            //dd(1);
            $header = curl_getinfo($ch)['http_code'] ?? 500;
            $return = $this->transformData($result, $header);
        } else {
            //dd($result);
            $return = $this->getError();
            //dd($return);
        }

        curl_close($ch);

        return $return;
    }
}