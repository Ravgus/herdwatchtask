<?php

namespace App\Command;

use App\Service\CurlService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GroupCommand extends Command
{
    private const GROUP_GET = 'get';
    private const GROUP_GET_ALL = 'get-all';
    private const GROUP_CREATE = 'create';
    private const GROUP_UPDATE = 'update';
    private const GROUP_DELETE = 'delete';

    protected static $defaultName = 'group';

    /**
     * @var string
     */
    private $serverUrl;

    public function __construct($serverUrl)
    {
        parent::__construct();

        $this->serverUrl = $serverUrl;
    }

    protected function configure()
    {
        $this
            ->addArgument('group-command', InputArgument::REQUIRED, 'Specify Command: get, get-all, create, update, delete')
            ->addArgument('group-data', InputArgument::REQUIRED, 'Specify group data: (name) or (id,name)')
            ->setDescription('Manage groups')
            ->setHelp('This command allows you to manipulate with group data');
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if ($input->getArgument('group-command') == self::GROUP_GET_ALL)
            $input->setArgument('group-data', null);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $command = $input->getArgument('group-command');
        $data = $input->getArgument('group-data');

        switch ($command) {
            case self::GROUP_GET:
                $this->getGroup($data, $output);
                break;
            case self::GROUP_GET_ALL:
                $this->getAllGroup($output);
                break;
            case self::GROUP_CREATE:
                $this->createGroup($data, $output);
                break;
            case self::GROUP_UPDATE:
                $this->updateGroup($data, $output);
                break;
            case self::GROUP_DELETE:
                $this->deleteGroup($data, $output);
                break;
            default:
                $output->writeln('Undefined command');
        }
    }

    public function getGroup($id, OutputInterface $output)
    {
        if (empty($id)) {
            $output->writeln('Undefined parameter "id"');
            return;
        }

        $request = new CurlService($this->serverUrl . '/groups/' . $id);
        $response = $request->getResponse('GET');

        $output->writeln($response);
    }

    public function getAllGroup(OutputInterface $output)
    {
        $request = new CurlService($this->serverUrl . '/groups/all');
        $response = $request->getResponse('GET');

        $output->writeln($response);
    }

    public function createGroup($name, OutputInterface $output)
    {
        $request = new CurlService($this->serverUrl . '/groups');
        $response = $request->getResponse('POST', ['name' => $name]);

        $output->writeln($response);
    }

    public function updateGroup($data, OutputInterface $output)
    {
        $data = explode(',', $data);

        if (empty($id = $data[0])) {
            $output->writeln('Undefined parameter "id"');
            return;
        }

        $groupData['name'] = $data[1] ?? null;

        $request = new CurlService($this->serverUrl . '/groups/' . $id);
        $response = $request->getResponse('PUT', $groupData);

        $output->writeln($response);
    }

    public function deleteGroup($id, OutputInterface $output)
    {
        if (empty($id)) {
            $output->writeln('Undefined parameter "id"');
            return;
        }

        $request = new CurlService($this->serverUrl . '/groups/' . $id);
        $response = $request->getResponse('DELETE');

        $output->writeln($response);
    }
}