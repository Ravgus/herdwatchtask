<?php

namespace App\Command;

use App\Service\CurlService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UserCommand extends Command
{
    private const USER_GET = 'get';
    private const USER_GET_ALL = 'get-all';
    private const USER_CREATE = 'create';
    private const USER_UPDATE = 'update';
    private const USER_DELETE = 'delete';

    protected static $defaultName = 'user';

    /**
     * @var string
     */
    private $serverUrl;

    public function __construct($serverUrl)
    {
        parent::__construct();
        
        $this->serverUrl = $serverUrl;
    }

    protected function configure()
    {
        $this
            ->addArgument('user-command', InputArgument::REQUIRED, 'Specify Command: get, get-all, create, update, delete')
            ->addArgument('user-data', InputArgument::REQUIRED, 'Specify user data: (name,email) or (id,name,email)')
            ->setDescription('Manage users')
            ->setHelp('This command allows you to manipulate with user data');
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if ($input->getArgument('user-command') == self::USER_GET_ALL)
            $input->setArgument('user-data', null);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $command = $input->getArgument('user-command');
        $data = $input->getArgument('user-data');

        switch ($command) {
            case self::USER_GET:
                $this->getUser($data, $output);
                break;
            case self::USER_GET_ALL:
                $this->getAllUser($output);
                break;
            case self::USER_CREATE:
                $this->createUser($data, $output);
                break;
            case self::USER_UPDATE:
                $this->updateUser($data, $output);
                break;
            case self::USER_DELETE:
                $this->deleteUser($data, $output);
                break;
            default:
                $output->writeln('Undefined command');
        }
    }

    public function getUser($id, OutputInterface $output)
    {
        if (empty($id)) {
            $output->writeln('Undefined parameter "id"');
            return;
        }

        $request = new CurlService($this->serverUrl . '/users/' . $id);
        $response = $request->getResponse('GET');

        $output->writeln($response);
    }

    public function getAllUser(OutputInterface $output)
    {
        $request = new CurlService($this->serverUrl . '/users/all');
        $response = $request->getResponse('GET');

        $output->writeln($response);
    }

    public function createUser($data, OutputInterface $output)
    {
        $data = explode(',', $data);

        $userData['name'] = $data[0] ?? null;
        $userData['email'] = $data[1] ?? null;

        $request = new CurlService($this->serverUrl . '/users');
        $response = $request->getResponse('POST', $userData);

        $output->writeln($response);
    }

    public function updateUser($data, OutputInterface $output)
    {
        $data = explode(',', $data);

        if (empty($id = $data[0])) {
            $output->writeln('Undefined parameter "id"');
            return;
        }

        $userData['name'] = $data[1] ?? null;
        $userData['email'] = $data[2] ?? null;

        $request = new CurlService($this->serverUrl . '/users/' . $id);
        $response = $request->getResponse('PUT', $userData);

        $output->writeln($response);
    }

    public function deleteUser($id, OutputInterface $output)
    {
        if (empty($id)) {
            $output->writeln('Undefined parameter "id"');
            return;
        }

        $request = new CurlService($this->serverUrl . '/users/' . $id);
        $response = $request->getResponse('DELETE');

        $output->writeln($response);
    }
}