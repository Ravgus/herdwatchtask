<?php

namespace App\Command;

use App\Service\CurlService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UserGroupCommand extends Command
{
    private const USER_GET = 'get';
    private const USER_ADD = 'add';
    private const USER_REMOVE = 'remove';

    protected static $defaultName = 'user-group';

    /**
     * @var string
     */
    private $serverUrl;

    public function __construct($serverUrl)
    {
        parent::__construct();

        $this->serverUrl = $serverUrl;
    }

    protected function configure()
    {
        $this
            ->addArgument('user-group-command', InputArgument::REQUIRED, 'Specify Command: get, add, remove')
            ->addArgument('user-group-data', InputArgument::REQUIRED, 'Specify user and group ids: (user_id) or (user_id,group_id)')
            ->setDescription('Add groups to specific user')
            ->setHelp('This command allows you to add groups to specific user');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $command = $input->getArgument('user-group-command');
        $data = $input->getArgument('user-group-data');

        switch ($command) {
            case self::USER_GET:
                $this->getUserGroup($data, $output);
                break;
            case self::USER_ADD:
                $this->addUserGroup($data, $output);
                break;
            case self::USER_REMOVE:
                $this->removeUserGroup($data, $output);
                break;
            default:
                $output->writeln('Undefined command');
        }
    }

    public function getUserGroup($id, OutputInterface $output)
    {
        if (empty($id)) {
            $output->writeln('Undefined parameter "id"');
            return;
        }

        $request = new CurlService($this->serverUrl . '/users/' . $id . '/groups');
        $response = $request->getResponse('GET');

        $output->writeln($response);
    }

    public function addUserGroup($data, OutputInterface $output)
    {
        $data = explode(',', $data);

        $user_id = $data[0] ?? null;
        $group_id = $data[1] ?? null;

        if (empty($user_id)) {
            $output->writeln('Undefined parameter "user id"');
            return;
        } elseif (empty($group_id)) {
            $output->writeln('Undefined parameter "group id"');
            return;
        }

        $request = new CurlService($this->serverUrl . '/users/' . $user_id . '/groups/' . $group_id);
        $response = $request->getResponse('POST');

        $output->writeln($response);
    }

    public function removeUserGroup($data, OutputInterface $output)
    {
        $data = explode(',', $data);

        $user_id = $data[0] ?? null;
        $group_id = $data[1] ?? null;

        if (empty($user_id)) {
            $output->writeln('Undefined parameter "user id"');
            return;
        } elseif (empty($group_id)) {
            $output->writeln('Undefined parameter "group id"');
            return;
        }

        $request = new CurlService($this->serverUrl . '/users/' . $user_id . '/groups/' . $group_id);
        $response = $request->getResponse('DELETE');

        $output->writeln($response);
    }
}