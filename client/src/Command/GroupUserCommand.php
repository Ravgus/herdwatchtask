<?php

namespace App\Command;

use App\Service\CurlService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GroupUserCommand extends Command
{
    private const GROUP_GET = 'get';
    private const GROUP_ADD = 'add';
    private const GROUP_REMOVE = 'remove';

    protected static $defaultName = 'group-user';

    /**
     * @var string
     */
    private $serverUrl;

    public function __construct($serverUrl)
    {
        parent::__construct();

        $this->serverUrl = $serverUrl;
    }

    protected function configure()
    {
        $this
            ->addArgument('group-user-command', InputArgument::REQUIRED, 'Specify Command: get, add, remove')
            ->addArgument('group-user-data', InputArgument::REQUIRED, 'Specify group and user ids: (group_id) or (group_id,user_id)')
            ->setDescription('Add users to specific group')
            ->setHelp('This command allows you to add users to specific group');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $command = $input->getArgument('group-user-command');
        $data = $input->getArgument('group-user-data');

        switch ($command) {
            case self::GROUP_GET:
                $this->getGroupUser($data, $output);
                break;
            case self::GROUP_ADD:
                $this->addGroupUser($data, $output);
                break;
            case self::GROUP_REMOVE:
                $this->removeGroupUser($data, $output);
                break;
            default:
                $output->writeln('Undefined command');
        }
    }

    public function getGroupUser($id, OutputInterface $output)
    {
        if (empty($id)) {
            $output->writeln('Undefined parameter "id"');
            return;
        }

        $request = new CurlService($this->serverUrl . '/groups/' . $id . '/users');
        $response = $request->getResponse('GET');

        $output->writeln($response);
    }

    public function addGroupUser($data, OutputInterface $output)
    {
        $data = explode(',', $data);

        $user_id = $data[1] ?? null;
        $group_id = $data[0] ?? null;

        if (empty($user_id)) {
            $output->writeln('Undefined parameter "user id"');
            return;
        } elseif (empty($group_id)) {
            $output->writeln('Undefined parameter "group id"');
            return;
        }

        $request = new CurlService($this->serverUrl . '/groups/' . $user_id . '/users/' . $group_id);
        $response = $request->getResponse('POST');

        $output->writeln($response);
    }

    public function removeGroupUser($data, OutputInterface $output)
    {
        $data = explode(',', $data);

        $user_id = $data[1] ?? null;
        $group_id = $data[0] ?? null;

        if (empty($user_id)) {
            $output->writeln('Undefined parameter "user id"');
            return;
        } elseif (empty($group_id)) {
            $output->writeln('Undefined parameter "group id"');
            return;
        }

        $request = new CurlService($this->serverUrl . '/groups/' . $user_id . '/users/' . $group_id);
        $response = $request->getResponse('DELETE');

        $output->writeln($response);
    }
}