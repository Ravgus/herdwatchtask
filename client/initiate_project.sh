#!/bin/bash

docker-compose up -d --build

docker exec -it client_php composer install

echo "Done"
