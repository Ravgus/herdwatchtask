ИНИЦИАЛИЗАЦИЯ ПРОЕКТА
1. Запустить ./initiate_project.sh
2. Создать .env файл по примеру .env.example
3. Комманды на api вводяться в консоли в корне проекта.
4. Синтаксис дан в описании комманд. 
К примеру, получим пользователя (1 = id):
> bin/app user get 1
К примеру, создадим пользователя (admin = name, admin@mail.com = email):
> bin/app user create admin,admin@mail.com
К примеру, обновим пользователя (1 = id, vasya = name, vasya@mail.com = email):
> bin/app user update 1,vasya,vasya@mail.com
К примеру, добавим пользователя к группе (1 = group_id, 2 = user_id):
> bin/app group-user add 1,2
