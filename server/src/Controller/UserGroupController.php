<?php

namespace App\Controller;

use App\Entity\Group;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class UserGroupController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param User $user
     * @return \FOS\RestBundle\View\View
     */
    public function read(User $user)
    {
        return $this->response($user->getGroups(), Response::HTTP_OK);
    }

    /**
     * @param User $user
     * @param Group $group
     * @return \FOS\RestBundle\View\View
     */
    public function create(User $user, Group $group)
    {
        $user->addGroup($group);

        $this->entityManager->flush();

        return $this->response('Ok', Response::HTTP_CREATED);
    }

    /**
     * @param User $user
     * @param Group $group
     * @return \FOS\RestBundle\View\View
     */
    public function delete(User $user, Group $group)
    {
        $user->removeGroup($group);

        $this->entityManager->flush();

        return $this->response('Ok', Response::HTTP_OK);
    }
}
