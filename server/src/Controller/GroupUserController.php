<?php

namespace App\Controller;

use App\Entity\Group;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class GroupUserController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Group $group
     * @return \FOS\RestBundle\View\View
     */
    public function read(Group $group)
    {
        return $this->response($group->getUsers(), Response::HTTP_OK);
    }

    /**
     * @param Group $group
     * @param User $user
     * @return \FOS\RestBundle\View\View
     */
    public function create(Group $group, User $user)
    {
        $group->addUser($user);

        $this->entityManager->flush();

        return $this->response('Ok', Response::HTTP_CREATED);
    }

    /**
     * @param Group $group
     * @param User $user
     * @return \FOS\RestBundle\View\View
     */
    public function delete(Group $group, User $user)
    {
        $user->removeGroup($group);

        $this->entityManager->flush();

        return $this->response('Ok', Response::HTTP_OK);
    }
}
