<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

abstract class BaseController extends AbstractFOSRestController
{
    /**
     * This method makes response for api application
     * @param string $message
     * @param int $code
     * @param array $headers
     * @return \FOS\RestBundle\View\View
     */
    public function response($message = '', int $code = Response::HTTP_OK, array $headers = [])
    {
        switch ($code) {
            case Response::HTTP_OK:
                return $this->view($message, Response::HTTP_OK, $headers);
            case Response::HTTP_CREATED:
                return $this->view($message, Response::HTTP_CREATED, $headers);
            case Response::HTTP_NO_CONTENT:
                return $this->view(null, Response::HTTP_NO_CONTENT, $headers);
            case Response::HTTP_BAD_REQUEST:
                return $this->view([
                    'error' => [
                        'message' => $message
                    ]
                ], Response::HTTP_BAD_REQUEST, $headers);
            case Response::HTTP_NOT_FOUND:
                return $this->view([
                    'error' => [
                        'message' => $message
                    ]
                ], Response::HTTP_NOT_FOUND, $headers);
            default:
                return $this->view([
                    'error' => [
                        'message' => 'Unexpected error'
                    ]
                ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * This method validates input data, and makes response, if data are invalid
     * @param ConstraintViolationListInterface $violations
     * @return bool|\FOS\RestBundle\View\View
     */
    public function getValidateResponse(ConstraintViolationListInterface $violations)
    {
        if (0 !== count($violations)) {
            foreach ($violations as $violation) {
                $errors[] = $violation->getMessage();
            }

            return $this->response($errors ?? [], Response::HTTP_BAD_REQUEST); //make a response with code 400
        }

        return true;
    }
}