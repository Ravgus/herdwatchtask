<?php

namespace App\Controller;

use App\Entity\Group;
use App\Repository\GroupRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

use \App\Model\Group as GroupModel;

class GroupController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var GroupRepository
     */
    private $groupRepository;
    /**
     * @var GroupModel
     */
    private $group;

    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        GroupRepository $groupRepository,
        GroupModel $group
    )
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->groupRepository = $groupRepository;
        $this->group = $group;
    }

    /**
     * @param Group $group
     * @return \FOS\RestBundle\View\View
     */
    public function read(Group $group)
    {
        return $this->response($group, Response::HTTP_OK);
    }

    /**
     * @return \FOS\RestBundle\View\View
     */
    public function readAll()
    {
        return $this->response($this->groupRepository->findAll(), Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return bool|\FOS\RestBundle\View\View
     */
    public function create(Request $request)
    {
        //get data from request
        $data = [
            'name' => $request->request->get('name')
        ];

        $group = $this->group->create($data); //create group

        //create validator for group and validate data
        $validate_results = $this->getValidateResponse($this->validator->validate($group));

        if (!is_bool($validate_results)) //if returns not true - incorrect data
            return $validate_results;

        $this->entityManager->persist($group);
        $this->entityManager->flush();

        return $this->response($group, Response::HTTP_CREATED);
    }

    /**
     * @param Group $group
     * @param Request $request
     * @return bool|\FOS\RestBundle\View\View
     */
    public function update(Group $group, Request $request)
    {
        //get data from request
        if ($request->request->has('name'))
            $data['name'] = $request->request->get('name');

        $group = $this->group->update($group, $data ?? []); //update group

        //create validator for group and validate data
        $validate_results = $this->getValidateResponse($this->validator->validate($group));

        if (!is_bool($validate_results)) //if returns not true - incorrect data
            return $validate_results;

        $this->entityManager->flush();

        return $this->response($group, Response::HTTP_OK);
    }

    /**
     * @param Group $group
     * @return \FOS\RestBundle\View\View
     */
    public function delete(Group $group)
    {
        $this->entityManager->remove($group);

        $this->entityManager->flush();

        return $this->response('Ok', Response::HTTP_OK);
    }
}
