<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

use \App\Model\User as UserModel;

class UserController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserModel
     */
    private $user;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        UserRepository $userRepository,
        UserModel $user
    )
    {
        $this->entityManager = $entityManager;
        $this->user = $user;
        $this->validator = $validator;
        $this->userRepository = $userRepository;
    }

    /**
     * @param User $user
     * @return \FOS\RestBundle\View\View
     */
    public function read(User $user)
    {
        return $this->response($user, Response::HTTP_OK);
    }

    /**
     * @return \FOS\RestBundle\View\View
     */
    public function readAll()
    {
        return $this->response($this->userRepository->findAll(), Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return bool|\FOS\RestBundle\View\View
     */
    public function create(Request $request)
    {
        //get data from request
        $data = [
            'email' => $request->request->get('email'),
            'name' => $request->request->get('name')
        ];

        $user = $this->user->create($data); //create user

        //create validator for user and validate data
        $validate_results = $this->getValidateResponse($this->validator->validate($user));

        if (!is_bool($validate_results)) //if returns not true - incorrect data
            return $validate_results;

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $this->response($user, Response::HTTP_CREATED);
    }

    /**
     * @param User $user
     * @param Request $request
     * @return bool|\FOS\RestBundle\View\View
     */
    public function update(User $user, Request $request)
    {
        //get data from request
        if ($request->request->has('email'))
            $data['email'] = $request->request->get('email');
        if ($request->request->has('name'))
            $data['name'] = $request->request->get('name');

        $user = $this->user->update($user, $data ?? []); //update user

        //create validator for user and validate data
        $validate_results = $this->getValidateResponse($this->validator->validate($user));

        if (!is_bool($validate_results)) //if returns not true - incorrect data
            return $validate_results;

        $this->entityManager->flush();

        return $this->response($user, Response::HTTP_OK);
    }

    /**
     * @param User $user
     * @return \FOS\RestBundle\View\View
     */
    public function delete(User $user)
    {
        $this->entityManager->remove($user);

        $this->entityManager->flush();

        return $this->response('Ok', Response::HTTP_OK);
    }
}
