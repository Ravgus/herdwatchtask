<?php

namespace App\Model;

use App\Entity\Group as EntityGroup;

class Group
{
    /**
     * Create Group
     * @param array $data
     * @return EntityGroup
     */
    public function create(array $data)
    {
        $group = new EntityGroup();

        $group->setName($data['name']);

        return $group;
    }

    /**
     * Update Group
     * @param EntityGroup $group
     * @param array $data
     * @return EntityGroup
     */
    public function update(EntityGroup $group, array $data)
    {
        if (array_key_exists('name', $data)) {
            $group->setName($data['name']);
        }

        return $group;
    }
}