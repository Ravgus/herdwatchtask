<?php

namespace App\Model;

use App\Entity\User as EntityUser;

class User
{
    /**
     * Create User
     * @param array $data
     * @return EntityUser
     */
    public function create(array $data)
    {
        $user = new EntityUser();

        $user->setEmail($data['email']);
        $user->setName($data['name']);

        return $user;
    }

    /**
     * Update User
     * @param EntityUser $user
     * @param array $data
     * @return EntityUser
     */
    public function update(EntityUser $user, array $data)
    {
        if (array_key_exists('email', $data)) {
            $user->setEmail($data['email']);
        }

        if (array_key_exists('name', $data)) {
            $user->setName($data['name']);
        }

        return $user;
    }
}