ИНИЦИАЛИЗАЦИЯ ПРОЕКТА
1. Создать .env файл по примеру .env.example
2. Запустить ./initiate_project.sh (если будут проблеммы при запуске, можно выполнить комманды из скрипта вручную по отдельности)
3. Опционально, импортировать коллекции роутов для postman (postman/TaskServer.postman_collection.json)
